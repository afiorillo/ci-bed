
build:
	docker build -t amfiorillo/ci-bed .

deploy: build
	docker tag amfiorillo/ci-bed amfiorillo/ci-bed:python-gae
	docker push amfiorillo/ci-bed:python-gae