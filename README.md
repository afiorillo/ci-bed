CI-Bed
---

A Docker image to speed up CI jobs. Bundled with:

- Debian Stretch
- Python 3.7
- Node 8
- Google Cloud SDK