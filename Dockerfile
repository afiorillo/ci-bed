FROM python:3.7
MAINTAINER Andrew Fiorillo <andrewmfiorillo@gmail.com>

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

# install pipenv, make, curl, and node8
RUN apt-get -qq update && \
    apt-get -qq install -y --no-install-recommends build-essential curl && \
    pip install pipenv && \
    curl -sL https://deb.nodesource.com/setup_8.x | bash - && \
    apt-get install -y nodejs && \
    rm -rf /var/lib/apt/lists/*
RUN mkdir -p "/etc/apt/sources.list.d/" && \
    echo "deb http://packages.cloud.google.com/apt cloud-sdk-stretch main" | tee "/etc/apt/sources.list.d/google-cloud-sdk.list" && \
    curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add - && \
    apt-get -qq update && \
    apt-get -qq install -y google-cloud-sdk && \
    rm -rf /var/lib/apt/lists/*